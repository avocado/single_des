#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "des.h"

void Printchar(unsigned char *Data, int Len) {
	int i;
	for (i=0; i<Len; i++) {
		printf("%c", Data[i]);
	}
	printf("\n");
}

void Printdex(unsigned char *Data, int Len) {
	int i;
	for (i=0; i<Len; i++) {
		printf("%02X ", Data[i]);
	}
	printf("\n");
}


int main(int argc, char* argv[]) {
	// ��Կ
	unsigned char key[] = {1,2,3,4,5,6,7,8};

	// ���Խ���
	//unsigned char TobeDec[] = {0x6A, 0x0F, 0x91, 0x92, 0x73, 0x46, 0x45, 0x70, 0x42, 0x8F, 0x49, 0x00, 0xB9, 0xB3, 0x8D, 0x0B};
	unsigned char TobeDec[] = {0x6A, 0x0F, 0x91, 0x92, 0x73, 0x46, 0x45, 0x70, 0x8F, 0x7E, 0xFD, 0x2D, 0x44, 0xB5, 0x23, 0x2D};
	unsigned char DecStr[32];
	memset(DecStr, 0, sizeof(DecStr));
	int DecLen = decrypt_data(key, TobeDec, sizeof(TobeDec)+8, DecStr);
	Printchar(DecStr, DecLen);

	// ���Լ���
	unsigned char msg[] = "hello,world.\n";
	unsigned char enc_msg[32];
	memset(enc_msg, 0, sizeof(enc_msg));
	int enc_len = encrypt_data(key, msg, sizeof(msg), enc_msg);
	Printdex(enc_msg, enc_len);

	// ���Խ���
	unsigned char outmsg[32];
	memset(outmsg, 0, sizeof(outmsg));
	int dec_len = decrypt_data(key, enc_msg, enc_len, outmsg);
	Printdex(outmsg, dec_len);
	Printchar(outmsg, dec_len);

	return 0;
}

/*
// �������ͼ�����Ϣ
void main_board(void) {
	unsigned char key[] = {1,2,3,4,5,6,7,8};
	unsigned char Msg[] = "hello,world.";
	unsigned char EncData[32];
	int MsgLen;	
	MsgLen = sizeof(Msg);
	if (MsgLen%8) {
		MsgLen |= 0x07;
		MsgLen++;
	}
	des_encrypt_ecb(Msg, EncData, MsgLen, key, 1);
	
	// ���ʹ�������
	SendCom(Msg, MsgLen);
}
*/
