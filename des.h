#ifndef _DES_H_
#define _DES_H_

// 加密函数
// key:密钥
// data:需要加密的数据
// len:需要加密数据的长度
// enc_str:加密后的数据
// 返回值:加密后数据的长度
int encrypt_data(unsigned char *key, unsigned char *data, int len, unsigned char *enc_str);

// 解密
// key:密钥
// enc_data:需要解密的数据
// enc_len:需要解密数据的长度
// dec_data:解密后在结果
// 返回值:解密后的长度
int decrypt_data(unsigned char *key, unsigned char *enc_data, int enc_len, unsigned char *dec_data);

#endif
